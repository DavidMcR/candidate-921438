package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import pages.HomeOffice;

public class RussianTouristStepDefinition {
	
	@Steps
	HomeOffice homeoffice;
	
	@Given("I provide a nationality of Russia")
	public void i_provide_a_nationality_of_Russia() {
		homeoffice.i_provide_a_nationality_of_Russia();
	}
	@Given("I select the reason “Tourism”")
	public void i_select_the_reason_Tourism() {
		homeoffice.i_select_the_reason_Tourism();
	}



	@Given("I state I am not travelling or visiting a partner or family")
	public void i_state_I_am_not_travelling_or_visiting_a_partner_or_family() {
		homeoffice.i_state_I_am_not_travelling_or_visiting_a_partner_or_family();
	}

	@Then("I will be informed “I need a visa to come to the UK”")
	public void i_will_be_informed_I_need_a_visa_to_come_to_the_UK() {
		homeoffice.i_will_be_informed_I_need_a_visa_Tourist();
	}

}
