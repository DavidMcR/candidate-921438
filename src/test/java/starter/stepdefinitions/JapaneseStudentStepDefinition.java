package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.HomeOffice;

public class JapaneseStudentStepDefinition {
	
	@Steps
	HomeOffice homeoffice;
	
	
	@Given("I provide a nationality of Japan")
	public void I_provide_a_nationality_of_Japan() {
		homeoffice.selectJapan();
		
	}
	@And("I select the reason “Study”")
	public void I_select_the_reason_Study() {
		homeoffice.I_select_the_reason_Study();
		
	}
	@And("I state I am intending to stay for more than {int} months")
	public void I_state_I_am_intending_to_stay_for_more_than_6_month(int months) {
		homeoffice.I_state_I_am_intending_to_stay_for_more_than_6_month();
	}
	
	@When("I submit the form")
	public void i_submit_the_form() {
		homeoffice.i_submit_the_form();
	
	}

	@Then("I will be informed “I need a visa to study in the UK”")
	public void i_will_be_informed_I_need_a_visa_to_study_in_the_UK() {
		homeoffice.i_will_be_informed_I_need_a_visa_to_come_to_the_UK();
	}

	

}
