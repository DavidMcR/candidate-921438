package pages;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Step;

public class HomeOffice {
	
	HomeOfficeSelection homepage;
	
	@Step
	public void openApplication() {
		homepage.open();
		
	}
	
	@Step
	public void selectJapan(){
		homepage.open();
		homepage.selectJapan();
	 }
		
	public void I_select_the_reason_Study() {
		homepage.studyReason();
	}
		
	public void I_state_I_am_intending_to_stay_for_more_than_6_month() {
		homepage.lengthofStudy();
	}
		
	
	public void i_submit_the_form(){
		homepage.submitForm();
	}
	
		public void i_will_be_informed_I_need_a_visa_to_study_in_the_UK()
		{
			homepage.verifyStudyNeedsVisa();
		}
		
		public void i_provide_a_nationality_of_Russia() {
			homepage.open();
			homepage.selectRussia();
		}
	
		public void i_select_the_reason_Tourism()
		{
			homepage.selectTourism();
		}
		
		public void i_state_I_am_not_travelling_or_visiting_a_partner_or_family() {
			homepage.selectNoFamily();
		}
		public void i_will_be_informed_I_need_a_visa_to_come_to_the_UK() {
			homepage.verifyStudyNeedsVisa();
			
		}
		public void i_will_be_informed_I_need_a_visa_Tourist() {
			homepage.verifyTouristNeedVisa();
			
		}

}
