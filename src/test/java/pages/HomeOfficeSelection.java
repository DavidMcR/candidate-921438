package pages;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import junit.framework.Assert;
import net.serenitybdd.core.pages.PageObject;

public class HomeOfficeSelection extends PageObject{

	public void selectJapan() {
		$(By.id("response")).selectByValue("japan");
		$(By.xpath("//*[@id=\"current-question\"]/button")).click();
		

	}
	public void studyReason() {
		$(By.id("response-2")).click();
		$(By.xpath("//*[@id=\"current-question\"]/button")).click();
	}
	public void lengthofStudy() {
		$(By.id("response-1")).click();
		
		
	}
	public void submitForm() {
		$(By.xpath("//*[@id=\"current-question\"]/button")).click();
	}
	
	public void selectRussia() {
		$(By.id("response")).selectByValue("russia");
		$(By.xpath("//*[@id=\"current-question\"]/button")).click();
	}
	
	public void selectTourism() {
		$(By.id("response-0")).click();
		$(By.xpath("//*[@id=\"current-question\"]/button")).click();
	}
	public void selectNoFamily() {
		$(By.id("response-1")).click();
		$(By.xpath("//*[@id=\"current-question\"]/button"));
	}
	public void verifyStudyNeedsVisa() {
		
		Assert.assertEquals($(By.xpath("//*[@id=\"result-info\"]/div[2]/h2")).getText(), "You’ll need a visa to study in the UK");
		
	}public void verifyTouristNeedVisa() {
		Assert.assertEquals($(By.xpath("//*[@id=\"result-info\"]/div[2]/h2")).getText(), "You’ll need a visa to come to the UK");
	}

}
